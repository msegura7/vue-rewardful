# README #

### Introduction ###

* This plugin adds the ability to easily use the (Rewardful)[https://app.getrewardful.com] javascript library in vue.js applications.
* To view available functions after installation you can go to (Rewardful's API page)[https://developers.getrewardful.com/javascript-api/overview].

### Set up ###

* Install the plugin as usual `npm install vue-rewardful`
* Include the script in the pages needed
  ```js
  import rewardful from "vue-rewardful";
  ```
* Pass your rewardful ID to the plugin
  ```js
  Vue.use(rewardful, {
    key: ######
  });
  ```

### Javascript-only use ###

  * Adding this code will make the Rewardful Cookie available.
  * This will open up the ability to use Rewardful's functions.
  * The most simple scenario would be
    * User comes to your site through your campaign url `https://www.domain.com?via=affiliate-name`
    * Rewardful cookie is set
    * User signs up
    * Place this code in the page you consider the user as *converted*
    ```js
      import rewardful from "vue-rewardful";
      Vue.use(rewardful, {
        key: ######
      });
      rewardful("convert", { email: "test@test.com" });
    ```
    * User is now a conversion. 


### Pass to server side ###

  * As per (Rewardful's instructions)[https://developers.getrewardful.com/javascript-api/overview#methods], you can attach a hidden input to a form by giving it the `data-rewardful=true` attribute 
  This:
    ```html
      <form action="/signup" method="post" data-rewardful="true">
        ...
      </form>
    ```
  Will result in:
  ```html
    <form action="/signup" method="post" data-rewardful="true">
    ...
    <!-- Inserted automatically by Rewardful: -->
    <input type="hidden" name="referral" value="####-#####-###">
    </form>
  ```
  * You can then capture the value and pass to your server or you can attach this to a Stripe form to handle with the Rewardful's Stripe integration
  


### Who do I talk to? ###


This plugin was designed and is maintained by (Aquila Development)[https://www.useaquila.dev/]

* Organization Page (Aquila Dev)[https://www.npmjs.com/org/aquila_dev]

* Repo Lead Developer Marco Segura
  * (NPM)[https://www.npmjs.com/~msegura]
  * (Bitbucket)[https://bitbucket.org/msegura7/]
  * (Github)[https://github.com/3birdsmedia]